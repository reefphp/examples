<?php

ini_set('display_errors', 'on'); error_reporting(E_ALL);

session_start();

require('../vendor/autoload.php');

// Specify which components we want to use
$Setup = new \Reef\ReefSetup(
	new Reef\Storage\NoStorageFactory(),
	new Reef\Layout\bootstrap5\bootstrap5(),
	new \Reef\Session\PhpSession()
);
$Setup->addComponent(new \Reef\Components\Upload\UploadComponent);
$Setup->addComponent(new \ReefExtra\Likert\LikertComponent);
$Setup->addExtension(new \ReefExtra\LabelTooltips\LabelTooltips);
$Setup->addExtension(new \ReefExtra\FontAwesome5\FontAwesome5);
$Setup->addExtension(new \ReefExtra\RequiredStars\RequiredStars);

$a_locales = ['en_US', 'nl_NL'];
if(isset($_GET['locale']) && ($i_pos = array_search($_GET['locale'], $a_locales)) !== false) {
	array_splice($a_locales, $i_pos, 1);
	array_unshift($a_locales, $_GET['locale']);
}

if(!is_dir('./storage/files')) {
	mkdir('./storage/files', 0777, true);
}

$Reef = new Reef\Reef(
	$Setup,
	[
		'cache_dir' => './cache/',
		'locales' => $a_locales,
		'internal_request_url' => './reefrequest.php?hash=[[request_hash]]',
		'files_dir' => './storage/files',
	]
);

if(!isset($_SESSION['sandbox'])) {
	$_SESSION['sandbox'] = [
		'definition' => [],
	];
}
