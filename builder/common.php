<?php

ini_set('display_errors', 'on'); error_reporting(E_ALL);

session_start();

require('../vendor/autoload.php');

$PDO = new \PDO("sqlite:storage/reef.db");
$PDO->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

// Specify which components we want to use
$Setup = new \Reef\ReefSetup(
	\Reef\Storage\PDOStorageFactory::createFactory($PDO),
	new Reef\Layout\bootstrap5\bootstrap5(),
	new \Reef\Session\PhpSession()
);
$Setup->addComponent(new \Reef\Components\Upload\UploadComponent);
$Setup->addComponent(new \ReefExtra\Likert\LikertComponent);
$Setup->addExtension(new \ReefExtra\LabelTooltips\LabelTooltips);
$Setup->addExtension(new \ReefExtra\FontAwesome5\FontAwesome5);
$Setup->addExtension(new \ReefExtra\RequiredStars\RequiredStars);

if(!is_dir('./storage/files')) {
	mkdir('./storage/files', 0777, true);
}

$Reef = new Reef\Reef(
	$Setup,
	[
		'cache_dir' => './cache/',
		'locales' => ['en_US', 'nl_NL'],
		'internal_request_url' => './reefrequest.php?hash=[[request_hash]]',
		'files_dir' => './storage/files',
	]
);
